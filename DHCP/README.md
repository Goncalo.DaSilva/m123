In dieser Dokumentation wird erklärt, wie man auf filiius einem DHCP Server aufsetzt und configuriert.

## DHCP Setup

Hier sieht man eine Stern Topologie mit 3 Laptops und und Client 3 hat eine Statische IP und 1/2 nicht. Eine Switch und ein DHCP Server sind auch vorhanden.

## Erklärung

Das ist ein Netzwerk, dass die IP Adressen vergeben kann muss man auf dem DHCP-Server draufdrücken. Danach muss man den DHCP konfigurieren.

## Schritt 1

Zuerst geht man auf die Laptops und klickt drauf. Nacher klickt man auf „DHCP zur Konfiguration verwenden“
Anschliessend muss man eine Statische IP Adresse eingeben und die Mac Adresse und Hinzufügen.

## Schritt 2

Beim PC-02 muss man bei der Grundeinstellungen die Adresse-Untergrenze und Obergrenze Eingeben die sieht man bei Bild 1 Grau geschrieben. IP Range für Clients.

## Fazit

Ich fand diese Arbeit sehr toll,da ich vieles neues über den DHCP-Server dazu gelernt habe.



## DHCP KONFIGURATION
1.Klicken Sie auf den dritten Laptop
 
2.Klicken Sie auf „DHCP-Server einrichten“
 
3. Klicken Sie den zweiten Tab an
 
4.Geben Sie die IP-Adresse und die Mac-Adresse ein
 
5. Wählen sie jetzt den zweiten Laptop aus und klicken Sie auf „DHCP-Server einrichten“.Anschließend übernehmen Sie diese Daten.
 
6.Wenn Sie mit dem fünften Schritt fertig sind, müssen Sie noch unten „DHCP aktivieren“ anklicken.
 
7.Wenn Sie alle Schritte sorgfältig durchgeführt haben, können sie auf den ersten Laptop klicken und dann unten rechts auf „DHCP zur Konfiguration verwenden“, wenn Sie dies getan haben, dann sind Sie fertig mit der DHCP Konfiguration.
 
Die DHCP Konfiguration hat mir persönlich sehr weiter geholfen, weil ich mich jetzt besser mit der DHCP Konfiguration auskenne.
M123 E-Portfolio
# GIT-Commands
- git status (aktueller Status)- git add . (fügt die veränderung hinzu)- git commit -m "Mitteilung" (übertragung zur Datenbank)- git push (vom Laptop zu Gitlab)- git pull (von Gitlab zum Laptop)
Ronaldo Champions League
 
Hier folgt der Text zum Modul 123
DCHP-Server
Was macht ein DHCP-Server genau?
Ein DHCP-Server (Dynamic Host Configuration Protocol) weist Geräten automatisch IP-Adressen im Netzwerk zu, wenn sie sich verbinden, erleichtert so die Netzwerkkonfiguration und optimiert die Kommunikation.
 
Welche Eigenschaften von einem DHCP-Server muss man kennen?
Automatische IP-Zuweisung: DHCP-Server ermöglichen die automatische Vergabe von IP-Adressen an Geräte in einem Netzwerk, was die manuelle Konfiguration vereinfacht.
Netzwerkkonfiguration: Neben IP-Adressen weist ein DHCP-Server auch Subnetzmasken, Standard     -Gateways und DNS-Server zu, um eine vollständige Netzwerkkonfiguration bereitzustellen.
Lease-Zeiten: DHCP vergibt temporäre IP-Adressen für einen bestimmten Zeitraum (Lease-Zeit). Geräte müssen ihre Lease erneuern, wenn sie weiterhin im Netzwerk aktiv sein wollen.
IP-Adresspools: DHCP-Server verwalten einen Pool von verfügbaren IP-Adressen. Diese Adressen werden den Geräten dynamisch zugewiesen und können nach Ablauf der Lease-Zeit wieder freigegeben werden.
Konflikterkennung: DHCP-Server überwachen das Netzwerk auf mögliche IP-Adresskonflikte, um sicherzustellen, dass jedem Gerät eine eindeutige Adresse zugewiesen wird.
DHCP-Relay: In grossen Netzwerken kann ein DHCP-Relay verwendet werden, um DHCP-Anfragen über verschiedene Subnetze hinweg weiterzuleiten.
Sicherheit: Um Missbrauch zu verhindern, sollte der Zugriff auf den DHCP-Server durch geeignete Sicherheitsmassnahmen geschützt werden.
Zuverlässigkeit: Ein zuverlässiger DHCP-Server ist entscheidend für die kontinuierliche Konnektivität im Netzwerk, Backup-Server können eingerichtet werden, um Ausfallzeiten zu minimieren.
Dora
Discover= Broadcast mit DHCP Discover
Offer= Jeder DHCP-Server prüft die Anfrage und antwortet mit DHCPOFFER --> gültige IP-Adresse wird übermittelt.
Request= Client empfängt Antwort und bestätigt die Auswahl dem Server
Acknowledge= Server trägt den Client mit der reservierten IP in die Datenbank ein.
Vorteile und Nachteile eines DHCP-Servers
Vorteile:


Flexibel bei Änderungen

Arbeitsersparnis (Kein Zugriff auf Clients)

Schutz vor Fehlkonfigurationen durch Benutzer

Optimale Ausnutzung des vorhandenen IP-Adresspools

Einfache Netzintegration neuer Clients

Keine IP-Adresskonflikte (Solange auf keinem Client statischen IP-Einträge im DHCP-Range vorhanden sind)
Nachteile


Grosse Auswirkung bei Fehlkonfiguration

Clients können einfach durch 2. DHCP-Server "gestört" werden (=Fehlkonfiguration auf Client pro Netzwerksegment)
Auftrag 1
1. Current index: 192.168.32.1
2. IP address range: 192.168.32.1 - 192.168.32.254
**3. Warum excluded man Adressen in einem DHCP Pool?**7 Acces Point, Dns, Router, Drucker, (Layer 3) Switch etc. Generell Dienstleistungen brauchen fixxe IP-Adressen. Ein Server hat immer eine fixxe IP-Adresse.
Aufgabe 3
192.168.32.30    0009.7CA6.4CE3           --                     Automatic
192.168.32.31    0001.632C.3508           --                     Automatic
192.168.32.32    0050.0F4E.1D82           --                     Automatic
192.168.32.33    00E0.8F4E.65AA           --                     Automatic
192.168.32.34    0007.ECB6.4534           --                     Automatic
192.168.32.35    00D0.BC52.B29B           --                     Automatic
4.Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?
Total addresses: 254
Prüfungsfragen:
Option 61: Mac Adresse
Option 53: DHCP Message Typen
Dora = Discover, Offer, Request, acknowledge
Grün= ARP Pakete(Layer 2) Vermittlung, überprüft die Verbindung von der IP-Adresse und Mac-Adresse
Blau= normale Pakete(Layer 3-7) Anwendung
Unterschied zwischen einem Server und einem normalen System:
Mehr Leistung, Redundanz (Hohe Verfügbarkeitsstatus)Ein redundanter Server ist ein Server, auf dem aktuell keine Kameradaten verarbeitet werden. Bei Ausfall eines Servers übernimmt ein redundanter Server dessen gesamte Kamera- und Geräteverarbeitung. Das bedeutet, dass alle Kameras auf den redundanten Server verschoben und von diesem verarbeitet werden.
Wenn man keine IP-Adresse vom DHCP-Server bekommt, erhält man eine Apipa Adresse , sie fängt immer mit "169" an.
Server Port 67
Client Port 68
Port 16 = Client
Port 17 = Server
Auftrag 2
Welcher OP-Code hat der DHCP-Offer?0x0000000000000002
Welcher OP-Code hat der DHCP-Request?x0000000000000001
Welcher OP-Code hat der DHCP-Acknowledge?x0000000000000002
An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?Es wird an die Broadcast gesendet. Dies ist die 255.255.255.255 An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell? FF.FF.FF.FF.FF.FF
Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?Weil es ein Broadcast ist und der an alle geschickt wird. Der Switch kann damit nichts anfangen und leitet es weiter.
 
 
 
[16:39] Gonçalo Da Silva Ribeiro

 
# M123 E-Portfolio
 
```python
# GIT-Commands
```
```
- git status (aktueller Status)
- git add . (fügt die veränderung hinzu)
- git commit -m "Mitteilung" (übertragung zur Datenbank)
- git push (vom Laptop zu Gitlab)
- git pull (von Gitlab zum Laptop)
```
Ronaldo Champions League
 
![ronaldo](./Images/ronaldo.png)
 
 
Hier folgt der Text zum Modul 123
 
# DCHP-Server
 
 
**Was macht ein DHCP-Server genau?**
 
Ein DHCP-Server (Dynamic Host Configuration Protocol) weist Geräten automatisch IP-Adressen im Netzwerk zu, wenn sie sich verbinden, erleichtert so die Netzwerkkonfiguration und optimiert die Kommunikation.
 
![DHCP-Server](./Images/OIP.png)
 
# Welche Eigenschaften von einem DHCP-Server muss man kennen?
**Automatische IP-Zuweisung**: DHCP-Server ermöglichen die automatische Vergabe von IP-Adressen an Geräte in einem Netzwerk, was die manuelle Konfiguration vereinfacht.    
 
**Netzwerkkonfiguration**: Neben IP-Adressen weist ein DHCP-Server auch Subnetzmasken, Standard     -Gateways und DNS-Server zu, um eine vollständige Netzwerkkonfiguration bereitzustellen.
 
**Lease-Zeiten**: DHCP vergibt temporäre IP-Adressen für einen bestimmten Zeitraum (Lease-Zeit). Geräte müssen ihre Lease erneuern, wenn sie weiterhin im Netzwerk aktiv sein wollen.
 
**IP-Adresspools**: DHCP-Server verwalten einen Pool von verfügbaren IP-Adressen. Diese Adressen werden den Geräten dynamisch zugewiesen und können nach Ablauf der Lease-Zeit wieder freigegeben werden.
 
**Konflikterkennung**: DHCP-Server überwachen das Netzwerk auf mögliche IP-Adresskonflikte, um sicherzustellen, dass jedem Gerät eine eindeutige Adresse zugewiesen wird.
 
**DHCP-Relay**: In grossen Netzwerken kann ein DHCP-Relay verwendet werden, um DHCP-Anfragen über verschiedene Subnetze hinweg weiterzuleiten.
 
**Sicherheit**: Um Missbrauch zu verhindern, sollte der Zugriff auf den DHCP-Server durch geeignete Sicherheitsmassnahmen geschützt werden.
 
**Zuverlässigkeit**: Ein zuverlässiger DHCP-Server ist entscheidend für die kontinuierliche Konnektivität im Netzwerk, Backup-Server können eingerichtet werden, um Ausfallzeiten zu minimieren.
 
# Dora
 
**Discover=** Broadcast mit DHCP Discover
 
**Offer=** Jeder DHCP-Server prüft die Anfrage und antwortet mit **DHCPOFFER -->** gültige IP-Adresse wird übermittelt.
 
**Request=** Client empfängt Antwort und bestätigt die Auswahl dem Server
 
**Acknowledge=** Server trägt den Client mit der reservierten IP in die Datenbank ein.
 
# Vorteile und Nachteile eines DHCP-Servers
**Vorteile:**
 
- Flexibel bei Änderungen
 
- Arbeitsersparnis (Kein Zugriff auf Clients)
 
- Schutz vor Fehlkonfigurationen durch Benutzer
 
- Optimale Ausnutzung des vorhandenen IP-Adresspools
 
- Einfache Netzintegration neuer Clients
 
- Keine IP-Adresskonflikte (Solange auf keinem Client statischen IP-Einträge im DHCP-Range vorhanden sind)
 
**Nachteile**
 
- Grosse Auswirkung bei Fehlkonfiguration
 
- Clients können einfach durch 2. DHCP-Server "gestört" werden (=Fehlkonfiguration auf Client pro Netzwerksegment)
 
# Auftrag 1
 
**1. Current index**: 192.168.32.1
 
**2. IP address range**: 192.168.32.1 - 192.168.32.254
 
**3. Warum excluded man Adressen in einem DHCP Pool?**7
Acces Point, Dns, Router, Drucker, (Layer 3) Switch etc. Generell Dienstleistungen brauchen fixxe IP-Adressen. Ein Server hat immer eine fixxe IP-Adresse.
 
**Aufgabe 3**
 
192.168.32.30    0009.7CA6.4CE3           --                     Automatic
 
192.168.32.31    0001.632C.3508           --                     Automatic
 
192.168.32.32    0050.0F4E.1D82           --                     Automatic
 
192.168.32.33    00E0.8F4E.65AA           --                     Automatic
 
192.168.32.34    0007.ECB6.4534           --                     Automatic
 
192.168.32.35    00D0.BC52.B29B           --                     Automatic
 
```javascript
4.Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?
```
 
**Total addresses:** 254
 
 # Prüfungsfragen:
 
 **Option 61**: Mac Adresse
 
 **Option 53**: DHCP Message Typen
 
 **Dora** = Discover, Offer, Request, acknowledge
 
 **Grün**= ARP Pakete(Layer 2) Vermittlung, überprüft die Verbindung von der IP-Adresse und Mac-Adresse
 
 **Blau**= normale Pakete(Layer 3-7) Anwendung
 
 ***Unterschied zwischen einem Server und einem normalen System***:
 
```python
Mehr Leistung, Redundanz (Hohe Verfügbarkeitsstatus)
 
Ein redundanter Server ist ein Server, auf dem aktuell keine Kameradaten verarbeitet werden. Bei Ausfall eines Servers übernimmt ein redundanter Server dessen gesamte Kamera- und Geräteverarbeitung. Das bedeutet, dass alle Kameras auf den redundanten Server verschoben und von diesem verarbeitet werden.
```
 
Wenn man keine IP-Adresse vom DHCP-Server bekommt, erhält man eine Apipa Adresse
, sie fängt immer mit "169" an.
 
**Server Port 67**
 
**Client Port 68**
 
**Port 16 = Client**
 
**Port 17 = Server**
 
# Auftrag 2
 
***Welcher OP-Code hat der DHCP-Offer?***
0x0000000000000002
 
***Welcher OP-Code hat der DHCP-Request?***
x0000000000000001
 
***Welcher OP-Code hat der DHCP-Acknowledge?***
x0000000000000002
 
***An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?***
Es wird an die Broadcast gesendet. Dies ist die 255.255.255.255
An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell? ***FF.FF.FF.FF.FF.FF***
 
***Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?***
Weil es ein Broadcast ist und der an alle geschickt wird. Der Switch kann damit nichts anfangen und leitet es weiter.