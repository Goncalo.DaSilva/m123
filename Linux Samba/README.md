[TOC]

## Samba Installation auf Linux

### Auftrag 
Wir erhielten den Auftrag SambaShare auf Linux zu installieren und einen Ordner mit dem Host teilen zu können.

### Einführung

Herr Ferrari und ich hatten uns zuerst eine passende Anleitung rausgesucht, die für unsere Version passt. Ich habe für Nevio diesen Part übernommen und habe eine sehr gute Anleitung gefunden, die uns zum Erflog gebracht hat.

### Konfiguration 

Als erstes haben wir den Ubuntu Desktop heruntergeladen und anschliessend in die VMware eingefügt. Die nächsten Konfigurationsschritte sind die üblichen, wenn man ein neues Gerät aufsetzt.

1. Sobald es fertig aufgesetzt ist, geht man in den Terminal und gint das erste Kommand ein. Mit diesen Kommand macht man alle Systemupdates.

![Alt text](<Bild (4)-1.png>)

2. Jetzt ist dieser schritt gekommen, wo man den Samba installiert.

![Alt text](<Screenshot 2024-01-15 151718.png>)

3. Somit bestätigen wir im System die Installation und es wird heruntergeladen.

![Alt text](image-2.png)

4. Wenn nach diesem Befehl eine grüne Bestätigung erscheint dann hat die Installation erfolgreich stattgefunden.

![Alt text](<Bild (2).png>)

5. Nun erlauben sie Samba den Firewall zugrief auf Ubuntu. Um über das SMB-Protokoll eine Verbindung herzustellen und auf die Freigegebenen Dateien zuzugreifen, haben wir zunächst eine Whitelist erstellt und den Zugriff auf den Dienst in der Firewall von ausserhalb des Computers zugelasst.

![Alt text](<Bild (7).png>)

6. Nun haben wir mit diesem Kommando einen User erstellt und in den nächsten Schritten ein passendes Passwort generiert.

![Alt text](image-3.png)

7. Wir sind nach diesen Schritten im Client eingeloggt und können mit dem Auftrag angefangen. Wir haben einen neuen Ordner erstellt und ihn nach "Samba" gennant.

![Alt text](<Bild (9).png>)

8. Damit wir dann auf diesen Ordner zugreifen können, muss man im lokalen Internet diesen Freigeben. Wichtig dabei ist, dass man den zweiten Haken auswählt, weil sonst die anderen (Client) nicht darauf zugreifen können.

![Alt text](<Bild (1).png>)

9. Jetzt gibt man diesen Ordner im lokalen Netzwerk frei. Man drückt auf "plus" und gibt smb://ip-adresse/name-des-freigegebenes-Ordner und klickt dan auf Verbinden.

![Alt text](<Bild (10).png>)

10. Zum schluss haben wir auf dem Client gewechselt und haben uns mit dem lokalen Netzwerk verbunden. Man geht auf den Explorer und macht einen Rechtsklick auf "Dieser PC" und drückt drauf "Netzwerkadresse" hinzufügen.

![Alt text](<bild101 (1).png>)

11. Als letztes haben wir wie im Screenshot zu sehen diese URL eingegeben, damit die interne Verbindung zu verfügung steht.

![Alt text](bild11.png)

### Reflexion

Dieser Auftrag ist uns Sehr gut gelungen, weil wir nie in schwierigkeiten gelangen sind und auch wenn haben wir uns gegenseitig unterstützt. Ich bin sehr zufrieden auf mich und Nevio, dass wir erstens eine gute Note erreicht haben und was mir noch wichtiger ist das unsere Chemie auf 100 war und wir ein sehr gutes Duo sind. Wir waren die ersten die diesen Auftrag erledigt haben und jedoch haben wir auch nocht etwas dazu gelernt.
