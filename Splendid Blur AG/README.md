# Filius Auftrag

### Einleitung
Ich erhielten den Auftrag, einen
Ich werde nicht jeden schritt denn ich gemacht habe hineinschreiben sondern nur den Ablauff meiner Arbeit.

### Vorbereitung
Als erstes habe ich die Filius Datei geöffnet die Geräte verbunden und die IP-Ranges wie auch der Websitename vom Webserver festgehalten.
Dies dient zur vereinfachten Konfiguration der Geräte.
Ausserdem habe ich sie schon bennant um verwechslungen entgegen zu wirken.

![Bild](Foto.1.png)

### SW01 Netzwerk - DHCP & 6 Clients
Als erstes habe ich die sechs Clients mit dem DHCP-Server eingerichtet.
Dies sind die IP-Einstellungen des DHCP-Servers:

Danach habe ich nach der Vorgegebenen Netzmaske den 

![Bild](Foto.2.png)

DHCP-Server eingerichtet:

Da einer der Sechs Clients eine Statische IP braucht habe ich den Client PC11 mit der MAC-Addresse eine Statische IP durch den DHCP-Server vergeben.
So sieht es dann aus:

Danach habe ich die Clients umgestellt so das sie ihre IP Informationen über den DHCP beziehen.


### SW03 Netzwerk - 2 Clients mit Statischer IP
Als zweites habe ich die beiden Clients beim SW03 Switch eingerichtet.

![Bild](Bild.4.png)

Hier müsste man nach vorgabe nicht mehr viel machen.

### SW02 Netzwerk - Web- & DNS-Server
Zuerst habe ich den Servern die IP Einstellungen eingerichtet.

![Bild](Foto.5.png)

### Web- & DNS-Server einrichten
Als erstes starten wir den Webserver. Wir müsses da wir es im Filius  machen, keine Website erstellen sondern nur den Dienst hinzufügen und starten.
Das macht man so:

1.
Webserver bei "Software-Installationen" installieren und öffnen.

2.
Auf "Starten" Drücken

![Bild](Foto.6-1.png)

Damit ist der Webserver Gestartet.

Jetzt müssen wir den DNS einrichten.
Ich habe bei mir die Webaddresse auf "splendid-blur.ch" mit der IP-Addresse 172.16.100.4 für meine Website benutzt.

![Bild](Foto.7.png)

Starten sie nach dem sie eine Addresse HInzugefügt haben denn Server und setzen sie das Häckchen.

### Web- & DNS-Server Testen
Um die beiden Server zu Testen müssen wir jetzt auf einen Client.
Da ich alle Clients Korrekt eingerichtet habe sollte jeder Client für die Tests Funktionieren.
Hier ist das Ergebniss von meinem versuch die Website Adresse einzugeben.

![Bild](Foto.11.png)

### Reflexion
Dieser Auftrag ist mir gut gelungen, weil ich nie in schwierigkeiten gelangen bin und auch wenn habe ich mir unterstützung geholt. Ich bin sehr zufrieden auf mich, dass ich diese Aufagbe erfolgreich erledigt habe. Leider war ich nicht der Schnellste jedoch habe ich etwas noch dazu gelernt.

